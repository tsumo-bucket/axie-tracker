<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'password' => 'required|max:191',
            'cms' => 'required',
            'verified' => 'required',
            'status' => 'required|in:active,blocked',
            'type' => 'required|in:super,admin,normal',
            'last_login' => 'max:191',
            'last_ip' => 'max:191',
            'remember_token' => 'max:100',
            'token' => 'max:191',
            'email_verified_at' => '',
            'user_role_id' => 'integer',
        ];
    }
}
